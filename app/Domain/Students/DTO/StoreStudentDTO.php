<?php

namespace App\Domain\Students\DTO;

class StoreStudentDTO
{
    /**
     * @var string
     */
    private string $fio;

    /**
     * @var string
     */
    private string $pinfl;

    /**
     * @var string
     */
    private string $phone;

    /**
     * @var string
     */
    private string $mac_address;

    /**
     * @param array $data
     * @return StoreStudentDTO
     */
    public static function fromArray(array $data): StoreStudentDTO
    {
        $dto = new self();
        $dto->setFio($data['fio']);
        $dto->setPinfl($data['pinfl']);
        $dto->setPhone($data['phone']);
        $dto->setMacAddress($data['mac_address']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     */
    public function setFio(string $fio): void
    {
        $this->fio = $fio;
    }

    /**
     * @return string
     */
    public function getPinfl(): string
    {
        return $this->pinfl;
    }

    /**
     * @param string $pinfl
     */
    public function setPinfl(string $pinfl): void
    {
        $this->pinfl = $pinfl;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMacAddress(): string
    {
        return $this->mac_address;
    }

    /**
     * @param string $mac_address
     */
    public function setMacAddress(string $mac_address): void
    {
        $this->mac_address = $mac_address;
    }
}
