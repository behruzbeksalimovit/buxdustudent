<?php

namespace App\Domain\Students\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $fillable = [
        'fio',
        'pinfl',
        'phone',
        'mac_address'
    ];
}
