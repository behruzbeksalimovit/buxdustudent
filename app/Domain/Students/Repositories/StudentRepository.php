<?php

namespace App\Domain\Students\Repositories;

use App\Domain\Students\Models\Student;
use Illuminate\Database\Eloquent\Collection;

class StudentRepository
{
    /**
     * @return Student[]|Collection
     */
    public function getAll()
    {
        return Student::orderBy('id','desc')->get();
    }
}
