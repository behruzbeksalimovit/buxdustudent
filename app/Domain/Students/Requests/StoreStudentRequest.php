<?php

namespace App\Domain\Students\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fio' => ['required','string','min:10'],
            'pinfl' => ['required','min:14','max:14','unique:students,pinfl'],
            'phone' => ['required', 'numeric','digits:12'],
            'mac_address' => ['required','unique:students,mac_address']
        ];
    }
}
