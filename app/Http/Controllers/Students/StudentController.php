<?php

namespace App\Http\Controllers\Students;

use App\Domain\Students\Actions\StoreStudentAction;
use App\Domain\Students\DTO\StoreStudentDTO;
use App\Domain\Students\Repositories\StudentRepository;
use App\Domain\Students\Requests\StoreStudentRequest;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class StudentController extends Controller
{
    /**
     * @var StudentRepository
     */
    public $students;

    /**
     * @param StudentRepository $studentRepository
     */
    public function __construct(StudentRepository $studentRepository)
    {
        $this->students = $studentRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json([
            'data' => $this->students->getAll()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStudentRequest $request
     * @param StoreStudentAction $action
     * @return JsonResponse
     */
    public function store(Request $request, StoreStudentAction $action): JsonResponse
    {
        try {
            $request->validate([
                'fio' => ['required', 'string', 'min:10'],
                'pinfl' => ['required', 'min:14', 'max:14', 'unique:students,pinfl'],
                'phone' => ['required','digits:12'],
                'mac_address' => ['required', 'unique:students,mac_address']
            ]);
        } catch (\Illuminate\Validation\ValidationException $validate) {
            return response()->json([
                'success' => false,
                'message' => $validate->getMessage(),
                'data' => $validate->validator->errors()->all()
            ]);
        }

        try {
            $dto = StoreStudentDTO::fromArray($request->all());
            $response = $action->execute($dto);
            return response()->json([
                'success' => true,
                'message' => 'Data created successfully.',
                'data' => $response,
            ], ResponseAlias::HTTP_OK);
        } catch (Exception $exception) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage(),
                'data' => []
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
